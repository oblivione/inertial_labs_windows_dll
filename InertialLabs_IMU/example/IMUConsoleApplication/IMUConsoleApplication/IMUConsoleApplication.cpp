#include <stdio.h>
//#include <windef.h>
#include <windows.h>
#include <tchar.h>
#include "InertialLabs_IMU.h"

#define IMU_OUTPUT_FORMAT IL_IMU_PSTABILIZATION_RECEIVE
/* Change the connection settings to your configuration. */
const char* COM_PORT = "COM1";
const int BAUD_RATE = 921600;


int main()
{
	IL_ERROR_CODE errorCode;
	IL_IMU imu;
	IMUCompositeData imu_data;;
	int i;

	errorCode = IMU_connect(
		&imu,
		COM_PORT,
		BAUD_RATE);

	/* Make sure the user has permission to use the COM port. */
	if (errorCode != ILERR_NO_ERROR)
	{
		printf("Could not connect to the sensor on this %s port error:%d\n did you add the user to the dialout group??? \n", COM_PORT, errorCode);

		return 0;
	}

	imu.cmd_flag = IL_STOP_CMD;
	errorCode = IMU_Stop(&imu);
	Sleep(3000);
	if (errorCode != ILERR_NO_ERROR)
	{
		printf("stop command error\n");

		return 0;
	}

	errorCode = IMU_SetMode(&imu, IL_SET_CONTINUES_MODE);

	if (errorCode != ILERR_NO_ERROR)
	{
		printf("Error encountered when setting the mode.\n");

		return 0;
	}

	if (imu.mode)
	{
		printf("On Request mode calibaration running \n");;
		Sleep(30000);
	}

	imu.cmd_flag = IMU_OUTPUT_FORMAT;

	errorCode = IMU_PStabilization_Receive(&imu);

	if (errorCode != ILERR_NO_ERROR)
	{
		printf("input data type error command error . \n");

		return 0;
	}

	Sleep(2000);
	while (1)
	{
#if 1
		IMU_YPR(&imu, &imu_data);

		IMU_getGyroAccMag(&imu, &imu_data);

		IMU_getSensorData(&imu, &imu_data);


		printf(" IMU data : \n");

		printf("  Yaw : %f \n Pitch : %f \n Roll : %f \n \n",
			imu_data.ypr.yaw, imu_data.ypr.pitch, imu_data.ypr.roll);

		printf("  Mag.x : %f \n Mag.y : %f \n Mag.z : %f \n "
			"Accel.x : %f \n Accel.y : %f \n Accel.z : %f\n "
			"Gyro.x : %f \n Gyro.y : %f \n Gyro.z : %f \n "
			"Vinp : %f \n Temper : %f \n \n",
			imu_data.magnetic.c0, imu_data.magnetic.c1, imu_data.magnetic.c2,
			imu_data.acceleration.c0, imu_data.acceleration.c1, imu_data.acceleration.c2,
			imu_data.gyro.c0, imu_data.gyro.c1, imu_data.gyro.c2, imu_data.Vinp, imu_data.Temper);
#endif

	}

	errorCode = IMU_disconnect(&imu);

	if (errorCode != ILERR_NO_ERROR)
	{
		printf("Error encountered when trying to disconnect from the sensor.\n");

		return 0;
	}

	return 0;
}
