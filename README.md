# inertial_labs_windows_dll

[![alt text](https://readthedocs.org/projects/docs/badge/?version=latest "Documentation Status")](https://oblivione.gitlab.io/inertiallabs_ros_package/index.html)

To run the examples code with INS device . then open the .sln file in `IntertialLabs_INS\example\INSConsoleApplication\` folder. Then build the solution then you can see the output after compiled .For an IMU-P device open the .sln file in `IntertialLabs_IMU\example\IMUConsoleApplication\`folder.


## To create your own app for ins or imu device using Visual Studio

 (In this explantion I took ins device to consider , you can replicate the same thing for a IMU-p device)

- On the menu bar, choose File > New > Project to open the Create a new project dialog box.

- At the top of the dialog, set Language to C++, set Platform to Windows, and set Project type to Console.

- From the filtered list of project types, choose Console App then choose Next.

- In the Configure your new project page, enter MathClient in the Project name box to specify a name for the project. Leave the default Location and Solution name values. Set Solution to Create new solution. Uncheck Place solution and project in the same directory if it's checked.

- Choose the Create button to create the client project.

###  Header to your include path

- Then in the `Solution Explorer` section , you can see  `Header Files` folder ,then right click on it , select `Add` -> `Existing Items...` .Then find the folder  `IntertialLabs_INS/include` .Include all the files in the folder.

- Right-click on the solution_name node in Solution Explorer to open the Property Pages dialog.

- In the Configuration drop-down box, select All Configurations if it's not already selected.

- In the left pane, select Configuration Properties > C/C++ > General.

- In the property pane, select the drop-down control next to the Additional Include Directories edit box, and then choose Edit.

- Double-click in the top pane of the Additional Include Directories dialog box to enable an edit control. Or, choose the folder icon to create a new entry.

- In the edit control, specify the path to the location of the  headers file you added in the first step . You can choose the ellipsis (...) control to browse to the correct folder.For example here add this to paths to your folder ,
`IntertialLabs_INS/include` and `IntertialLabs_INS/lib`.

### Import library to your project

- Right-click on the solution_name node in Solution Explorer and choose Properties to open the Property Pages dialog.

- In the Configuration drop-down box, select All Configurations if it's not already selected. It ensures that any property changes apply to both Debug and Release builds.

- In the left pane, select Configuration Properties > Linker > Input. In the property pane, select the drop-down control next to the Additional Dependencies edit box, and then choose Edit.

- In the Additional Dependencies dialog, add .lib to the list in the top edit control.For ins you can add IntertialLabs_INS.lib 

- Choose OK to go back to the Property Pages dialog box.

- In the left pane, select Configuration Properties > Linker > General. In the property pane, select the drop-down control next to the Additional Library Directories edit box, and then choose Edit.

- Double-click in the top pane of the Additional Library Directories dialog box to enable an edit control. In the edit control, specify the path to the location of the IntertialLabs_INS.lib file .

- Once you've entered the path to the library file in the Additional Library Directories dialog box, choose the OK button to go back to the Property Pages dialog box. Choose OK to save the property changes.

### Congrats!!! your platform is ready now 

- Now you can write the code , `Solution Explorer` section , you can see  `Source Files` folder . You can find the alredy generated cpp file , you can add the code there using the headers .
look at the examples we provide how to write code 

 `IntertialLabs_INS/example/INSConsoleApplication/INSConsoleApplication/INSConsoleApplication.cpp`
 `IntertialLabs_IMU/example/IMUConsoleApplication/IMUConsoleApplication/IMUConsoleApplication.cpp`

  make sure the solution platform is set to X86 when you run the program. Now you can build the your own solution. 

### Reference 

- Want to see, How to run the example we provided for an ins device? 
    
    Go to the video folder and play the run_`example_ins.mp4`, you can see the whole process there. At the end of 30-50 seconds you can see the paths we already set for the example to work . you use the example code directly with the device  those paths you can see specific to the given example .
    
## Bug Report

Prefer to open an issue. You can also send an E-mail to omprakashpatro@gmail.com.
