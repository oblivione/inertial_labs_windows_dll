#include <stdio.h>
//#include <windef.h>
#include <windows.h>
#include <tchar.h>
#include "InertialLabs_INS.h"

#define INS_OUTPUT_FORMAT IL_OPVT_RECEIVE
/* Change the connection settings to your configuration. */

const char* COM_PORT = "COM4";
const int BAUD_RATE = 460800;


int main()
{
	IL_ERROR_CODE errorCode;
	IL_INS ins;
	INSCompositeData sensor_data;
	INSPositionData pos_data;
	int i;

	memset(&sensor_data, 0, sizeof(sensor_data));

	memset(&pos_data, 0, sizeof(pos_data));

	errorCode = INS_connect(
		&ins,
		COM_PORT,
		BAUD_RATE);

	/* Make sure the user has permission to use the COM port. */
	if (errorCode != ILERR_NO_ERROR)
	{
		printf("Could not connect to the sensor on this %s port error:%d\n did you add the user to the dialout group??? \n", COM_PORT, errorCode);

		return 0;
	}

	ins.cmd_flag = IL_STOP_CMD;
	errorCode = INS_Stop(&ins);
	Sleep(3000);

	if (errorCode != ILERR_NO_ERROR)
	{
		printf("stop command error\n");

		return 0;
	}

	errorCode = INS_SetMode(&ins, IL_SET_CONTINUES_MODE);

	if (errorCode != ILERR_NO_ERROR)
	{
		printf("Error encountered when setting the mode.\n");

		return 0;
	}

	ins.cmd_flag = IL_OPVT_RECEIVE;
	printf("Yaw, Pitch, Roll\n");

	errorCode = INS_OPVTdata_Receive(&ins);

	if (errorCode == ILERR_DATA_IN_BUFFER)
	{
		printf("input data type error command error . \n");

		return 0;
	}

	Sleep(2000);
	printf("[INFO]Initial Alignment Started!!! \n");
	while (1)
	{
		/*this is the example to see how you can check that there data start coming to the buffer in the output dataformat*/
		errorCode = INS_YPR(&ins, &sensor_data);
		if (errorCode == ILERR_NO_ERROR)
		{
			printf(" Head  : %f \t\t\n  Pitch : %f \t\t\n  Roll : %f  \t\t \n\n\n", sensor_data.ypr.yaw, sensor_data.ypr.pitch, sensor_data.ypr.roll);
		}
		else if (errorCode == ILERR_MEMORY_ERROR)
		{
			//printf("there no data in the buffer for this output format , initial alighment running!!!");
		}

		errorCode = INS_getGyroAccMag(&ins, &sensor_data);

		if (errorCode == ILERR_NO_ERROR)
		{
			printf("  Mag.x : %f \t\t\n  Mag.y : %f \t\t\n Mag.z : %f \t\t\n  Accel.x : %f \t\t\n  Accel.y : %f \t\t\n Accel.z : %f \t\t\n Gyro.x : %f \t\t\n Gyro.y %f \t\t\n Gyro.z : %f \t  \n\n",
				sensor_data.magnetic.c0, sensor_data.magnetic.c1, sensor_data.magnetic.c2,
				sensor_data.acceleration.c0, sensor_data.acceleration.c1, sensor_data.acceleration.c2,
				sensor_data.gyro.c0, sensor_data.gyro.c1, sensor_data.gyro.c2);
		}
		else if (errorCode == ILERR_MEMORY_ERROR)
		{
			//printf("there no data in the buffer for this output format , initial alighment running!!!");
		}


		//INS_getPositionData(&ins, &pos_data);


	   //printf(" sensor data : \n");
	   //printf(" Head \t\t Pitch \t\t Roll \t\t  Mag.x\t\t Mag.y\t\t Mag.z\t\t Accel.x\t\t Accel.y\t\t Accel.z\t\t Gyro.x\t\t Gyro.y\t\t Gyro.z\t \n");
	   //printf("  %f\t\t %f\t\t  %f\t\t %f\t\t  %f\t\t  %f\t\t %f\t\t %f\t\t %f\t\t %f\t\t %f\t\t %f\t \n\n", sensor_data.ypr.yaw, sensor_data.ypr.pitch, sensor_data.ypr.roll,
	   //	sensor_data.magnetic.c0, sensor_data.magnetic.c1, sensor_data.magnetic.c2,
	   //	sensor_data.acceleration.c0, sensor_data.acceleration.c1, sensor_data.acceleration.c2,
	   ///	sensor_data.gyro.c0, sensor_data.gyro.c1, sensor_data.gyro.c2);

	   /* Wait for 1 second before we query the sensor again. */

	   //Sleep(500);

	}

	errorCode = INS_disconnect(&ins);

	if (errorCode != ILERR_NO_ERROR)
	{
		printf("Error encountered when trying to disconnect from the sensor.\n");

		return 0;
	}

	return 0;
}
