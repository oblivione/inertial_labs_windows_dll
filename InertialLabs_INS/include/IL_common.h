/**
 * \file

 * \section DESCRIPTION
 * This header file provides access to features commonly used throughout the
 * entire InertialLabs C/C++ Library.
 */
#ifndef _IL_COMMON_H_
#define _IL_COMMON_H_

int IL_getErrorCode();

#define IL_DBG 0                   /**< SDK Debug mode off. */
#define IL_RAW_DATA 0
#define IL_DECODE_DATA 0
#endif /* _IL_COMMON_H_ */